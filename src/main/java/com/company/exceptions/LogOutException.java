package com.company.exceptions;

public class LogOutException extends Exception {
    public LogOutException() {
        super("You log out");
    }
}
