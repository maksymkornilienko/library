package com.company;

import com.company.dataprovider.ConsoleDataProvider;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        ConsoleDataProvider consoleDataProvider = new ConsoleDataProvider();
        consoleDataProvider.checkPassword();
    }
}
