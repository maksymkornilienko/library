package com.company.library;

import com.company.access.Accesses;
import com.company.dataprovider.ConsoleDataProvider;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Books {

    private static Map<Integer, String> ownBooksList;
    private static Map<Integer, String> booksList;
    private static List<Book> books;
    private static int countBooks = 0;

    public static void ownCabinet(String username, List<Book> books) {
        ownBooksList = new HashMap<Integer, String>();
        for (Book book : books) {
            if (book.getUsername().equals(username)) {
                ownBooksList.put(book.getId(), book.getName());
            }
        }
        if (!ownBooksList.isEmpty()) {
            System.out.println("This books which do you have: ");
            ownBooksList.forEach((k, v) -> {
                System.out.println(k + "=>" + v);
            });
        } else {
            System.out.println("You don't have any books");
        }

    }
    public static boolean countOwnBooks(String username, List<Book> books) {
        for (Book book : books) {
            if (book.getUsername().equals(username)) {
                countBooks++;
            }
        }
        String role=Accesses.getRole(username);
        return (role.equals("reader") && countBooks == 0) || (role.equals("admin") && countBooks < 2 && countBooks >= 0);
    }

    public static void readBooks(List<Book> books) {
        booksList = new HashMap<Integer, String>();
        for (Book book : books) {
            if (book.getStatus() == 0) {
                booksList.put(book.getId(), book.getName());
            }
        }
        System.out.println("Choose a book which you want to take :");
        booksList.forEach((k, v) -> System.out.println(k + "=>" + v));
    }

    public static void getBook(int id, String username, List<Book> books) throws IOException {
        String check=booksList.get(id);
        if (check!=null && !check.isEmpty()) {
            CSVWriter writer = Books.writeCsv();
            for (Book book : books) {
                if (id == book.getId() && book.getStatus()==0 && book.getUsername().equals("nobody")) {
                    book.setStatus(1);
                    book.setUsername(username);
                    System.out.println("You take a book " + book.getName());
                }
                String[] data = {String.valueOf(book.getId()), book.getName(), String.valueOf(book.getStatus()), book.getUsername()};
                writer.writeNext(data);
            }
            writer.close();
        }
    }

    public static void returnBook(int id, String username, List<Book> books) throws IOException {
        String check=ownBooksList.get(id);
        if (!check.isEmpty()) {
            CSVWriter writer = Books.writeCsv();
            for (Book book : books) {
                if (id == book.getId() && book.getStatus()==1 && book.getUsername().equals(username)) {
                    book.setStatus(0);
                    book.setUsername("nobody");
                    ownBooksList.remove(id);
                    System.out.println("The book " + book.getName() + " was return");
                }
                String[] data = {String.valueOf(book.getId()), book.getName(), String.valueOf(book.getStatus()), book.getUsername()};
                writer.writeNext(data);
            }
            writer.close();
        }
    }
    public static List<Book> readCsv() throws IOException {
        books = new ArrayList<Book>();
        File file = new File("src/main/java/books.csv");
        FileReader outputfile = new FileReader(file);
        CSVReader reader = new CSVReader(outputfile, ';', CSVWriter.NO_QUOTE_CHARACTER);
        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            books.add(new Book(Integer.parseInt(nextLine[0]), nextLine[1], Integer.parseInt(nextLine[2]), nextLine[3]));
        }
        return books;
    }
    public static CSVWriter writeCsv() throws IOException {
        File file = new File("src/main/java/books.csv");
        FileWriter outputfile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputfile, ';', CSVWriter.NO_QUOTE_CHARACTER);
        return writer;
    }
}
