package com.company.library;

public class Book {
    private int id;
    private int status;
    private String name;
    private String username;

    public Book(int id, String name, int status, String username) {
        this.id = id;
        this.status = status;
        this.name = name;
        this.username = username;
    }
    public String getName() {
        return name;
    }

    public int getStatus() {
        return status;
    }

    public String getUsername() {
        return username;
    }

    public int getId() {
        return id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
