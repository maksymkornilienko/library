package com.company.access;

import com.company.Main;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Access {
    private int id;
    private String username;
    private String password;
    private String role;

    public Access(int id, String username, String password, String role){
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
