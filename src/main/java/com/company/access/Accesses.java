package com.company.access;

import com.company.library.Book;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Accesses {
    private static List<Access> users;

    public static synchronized void readUsers() throws IOException {
        String csvFile = "src/main/java/users.csv";
        BufferedReader br = null;
        if(null == users){
            users = new ArrayList<Access>();
            br = new BufferedReader(new FileReader(csvFile));
            String line;
            while((line = br.readLine()) != null){
                String[] tokens = line.split(";");
                users.add(new Access(Integer.parseInt(tokens[0]), tokens[1], tokens[2],tokens[3]));
            }
        }
    }
    public static synchronized boolean find(String username, String password){
        if(null == users){
            throw new IllegalStateException("user list is not initialised");
        }

        return users.stream()
                .filter(u -> u.getUsername().equals(username))
                .filter(u -> u.getPassword().equals(password))
                .findFirst()
                .isPresent();
    }
    public static String getRole(String username){
        String role="";
        for (Access user : users) {
            if (user.getUsername().equals(username)) {
                role = user.getRole();
            }
        }
        return role;
    }
}
