package com.company.dataprovider;


import com.company.access.Access;
import com.company.library.Book;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface DataProvider {
    void checkPassword() throws IOException;
    void getAction(String username) throws IOException;
    void actionGet(String username, boolean checkCount, List<Book> books) throws IOException;
    void actionReturn(String username, List<Book> books) throws IOException;
}
