package com.company.dataprovider;

import com.company.access.Access;
import com.company.access.Accesses;
import com.company.exceptions.CheckLoginException;
import com.company.exceptions.LogOutException;
import com.company.exceptions.OwnCabinetException;
import com.company.library.Book;
import com.company.library.Books;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.io.*;
import java.util.*;

public class ConsoleDataProvider implements DataProvider {
    private static List<Book> books;
    public String username;
    public String password;
    private static Scanner scanner = new Scanner(System.in);

    @Override
    public void checkPassword() throws IOException {
        System.out.println("Log in:");
        System.out.println("username: ");
        this.username = scanner.next();
        System.out.println("password: ");
        this.password = scanner.next();
        try {
            Accesses.readUsers();
            boolean check = Accesses.find(this.username, this.password);
            if (check){
                getAction(this.username);
            }
            throw new CheckLoginException();
        } catch (CheckLoginException e) {
            System.err.println(e.getMessage());
            checkPassword();
        }
    }


    @Override
    public void getAction(String username) throws IOException {
        int bookId;
        int changeAction = 0;
        boolean checkCount;
        books = Books.readCsv();
        System.out.println("Welcome " + username);
        Books.ownCabinet(username, books);
        checkCount = Books.countOwnBooks(username, books);
        if (checkCount) {
            System.out.println("Choose action \n 1-take book \n 2-return book\n 3-log out");
        } else {
            System.out.println("Choose action \n 2-return book\n 3-log out");
        }
        changeAction = scanner.nextInt();
        try {
            if (changeAction == 1) {
                actionGet(this.username, checkCount, books);
            } else if (changeAction == 2) {
                actionReturn(this.username, books);
            }else{
                throw new LogOutException();
            }
        } catch (LogOutException e) {
            System.err.println(e.getMessage());
            checkPassword();
        }
    }

    @Override
    public void actionGet(String username, boolean checkCount, List<Book> books) throws IOException {
        try {
            if (checkCount) {
                Books.readBooks(books);
                int bookId = scanner.nextInt();
                Books.getBook(bookId, username, books);
            }
            throw new OwnCabinetException();
        } catch (OwnCabinetException e) {
            getAction(username);
        }
    }

    @Override
    public void actionReturn(String username, List<Book> books) throws IOException {
        Books.ownCabinet(username, books);
        System.out.println("Which book do you want return?");
        int bookId = scanner.nextInt();
        try {
            Books.returnBook(bookId, username, books);
            throw new OwnCabinetException();
        } catch (OwnCabinetException e) {
            getAction(username);
        }
    }
}
